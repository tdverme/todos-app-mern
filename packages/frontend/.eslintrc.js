module.exports = {
  "root": true,
  "env": {
    "browser": true,
    "es2021": true
  },
  "extends": [
    "plugin:react/recommended",
  ],
  "overrides": [
    {
      "files": [
        "*.ts",
        "*.tsx"
      ],
      "extends": [
        "plugin:@typescript-eslint/recommended",
        "plugin:@typescript-eslint/recommended-requiring-type-checking",
        "plugin:react/jsx-runtime"
      ],
      "parserOptions": {
        "tsconfigRootDir": __dirname,
        "project": [
          "./tsconfig.json"
        ],
      }
    }
  ],
  "plugins": [
    "react",
    "react-hooks",
    "import"
  ],
  "settings": {
    "import/parsers": {
      "@typescript-eslint/parser": [
        ".ts",
        ".tsx"
      ]
    },
    "import/resolver": {
      "typescript": {
        "alwaysTryTypes": true,
        "project": "./tsconfig.json"
      }
    },
    "rules": {
      "react/jsx-uses-react": "off",
      "react/react-in-jsx-scope": "off",
      "import/no-unresolved": "error",
      "no-use-before-define": "off",
      "@typescript-eslint/no-use-before-define": [
        "error"
      ],
      "react/jsx-filename-extension": [
        "warn",
        {
          "extensions": [
            ".tsx"
          ]
        }
      ],
      "import/extensions": [
        "error",
        "ignorePackages",
        {
          "js": "never",
          "jsx": "never",
          "ts": "never",
          "tsx": "never"
        }
      ],
      "no-shadow": "off",
      "@typescript-eslint/no-shadow": [
        "error"
      ],
      "@typescript-eslint/explicit-function-return-type": [
        "error",
        {
          "allowExpressions": true
        }
      ],
      "react-hooks/rules-of-hooks": "error",
      "react-hooks/exhaustive-deps": "warn",
      "import/prefer-default-export": "off",
      "indent": "off",
      "react/jsx-indent": "off",
      "@typescript-eslint/no-unnecessary-condition": false,
      "@typescript-eslint/no-unsafe-member-access": 'off'
    }
  }
}