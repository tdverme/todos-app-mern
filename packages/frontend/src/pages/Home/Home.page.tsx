/* eslint-disable @typescript-eslint/no-misused-promises */

import axios, { AxiosResponse } from "axios";
import TodoForm from "components/TodoForm/TodoForm";
import { useState, useEffect } from "react";
import { Todo } from "types/Todo";
import { Divider, HomePageContainer, HomePageTitle, LogoutButton, LogoutContainer } from "./Home.styled";
import TodoItemList from "components/TodoItemList/TodoItemList";
import { compareTodos } from "utilities/compareTodos";
import { useNavigate } from "react-router-dom";

const TodoPage = () => {
  const jwt = localStorage.getItem("token");

  const [todos, setTodos] = useState<Todo[]>([]);
  const [error, setError] = useState("");

  const navigate = useNavigate()

  useEffect(() => {
    if (!jwt) {
      navigate('/login')
    }
  }, [jwt])

  useEffect(() => {
    const fetchTodos = async () => {
      try {
        const { data: todos } = await axios.get<Todo[]>(
          "/api/todos",
          { headers: { Authorization: `Bearer ${jwt || ""}` } }
        );

        setTodos(todos.sort(compareTodos));
      } catch (error) {
        console.error(error);
        setError("Failed to fetch todos");
      }
    };

    fetchTodos().catch(() => console.error("Failed to fetch todos"));
  }, []);

  const handleAddTodo = async (title: string, description: string) => {
    try {
      const { data: newTodo } = await axios.post<Todo, AxiosResponse<Todo>>(
        "/api/todos",
        { title, description, completed: false },
        { headers: { Authorization: `Bearer ${jwt || ""}` } }
      );

      setTodos([...todos, newTodo].sort(compareTodos));
    } catch (error) {
      console.error(error);
      setError("Failed to add todo");
    }
  };

  const handleLogout = () => {
    localStorage.removeItem("token");
    navigate("/")
  };

  return (
    <HomePageContainer>
      <LogoutContainer>
        <LogoutButton onClick={handleLogout}>Salir</LogoutButton>
      </LogoutContainer>

      <HomePageTitle>Home</HomePageTitle>

      <TodoForm onSubmit={handleAddTodo} />

      <Divider />

      {error && <div>{error}</div>}
      {!error && <TodoItemList todos={todos} setTodos={setTodos} />}
    </HomePageContainer>
  );
};

export default TodoPage;
