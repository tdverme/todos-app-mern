import { styled } from "styled-components";

export const HomePageContainer = styled.div`
  padding: 0 4em;

  display: flex;
  flex-direction: column;
  gap: 2em;
  align-items: center;
`;

export const LogoutContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  padding: 1em 0 0 0;
`

export const LogoutButton = styled.button`
  width: min-content;
  border-radius: 0.25em;
  white-space: nowrap;
  padding: 0.75em 1.25em;
  border: none;
  background-color: #FB3B1E;
  color: white;
  font-size: 12px;
`

export const HomePageTitle = styled.h1`
  font-size: 28px;
  font-weight: bold;
`;

export const Divider = styled.hr`
  border-top: 1px solid #eeeeee;
  width: 100%;
`