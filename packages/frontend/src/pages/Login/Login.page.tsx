/* eslint-disable @typescript-eslint/no-misused-promises */
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { LoginFormContainer, LoginFormTitle, LoginFormInputContainer, LoginFormLabel, LoginFormInput, LoginFormButton, ErrorMessage } from "./Login.styled";

interface ILoginResponse {
  token: string
}

const LoginPage = (): React.ReactElement => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  const navigate = useNavigate()

  const handleLogin = async (e: React.FormEvent) => {
    e.preventDefault();

    try {
      const response = await axios.post<ILoginResponse>("/api/login", { username, password });
      const { token } = response.data;

      localStorage.setItem("token", token);

      setUsername("");
      setPassword("");
      setError("");

      navigate("/")
    } catch (error) {
      setError("Credenciales incorrectas");
    }
  };

  useEffect(() => {
    const isLoggedIn = !!localStorage.getItem("token");

    if (isLoggedIn) {
      navigate("/")
    }
  }, [])


  return (
    <LoginFormContainer onSubmit={handleLogin}>
      <LoginFormTitle>Iniciar Sesión</LoginFormTitle>

      <LoginFormInputContainer>
        <LoginFormLabel>Usuario</LoginFormLabel>
        <LoginFormInput
          id="user"
          placeholder="Usuario"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
      </LoginFormInputContainer>

      <LoginFormInputContainer>
        <LoginFormLabel>Contraseña</LoginFormLabel>
        <LoginFormInput
          id="password"
          type="password"
          placeholder="Contraseña"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </LoginFormInputContainer>

      {error && <ErrorMessage>{error}</ErrorMessage>}

      <LoginFormButton type="submit">Ingresar</LoginFormButton>
    </LoginFormContainer>
  )

}

export default LoginPage