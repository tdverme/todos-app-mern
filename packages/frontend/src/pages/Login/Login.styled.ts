import styled from "styled-components"

export const LoginFormContainer = styled.form`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 0.25em;
`

export const LoginFormTitle = styled.h4`
  margin-bottom: 0.75em;
`

export const LoginFormInputContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.25em;
  margin-bottom: 0.5em;
`

export const LoginFormLabel = styled.label`
  font-size: 10px;
  font-weight: normal;
  color: #2F2F2F;
`

export const LoginFormInput = styled.input`
  width: 250px;
  padding: 0.5em;
  font-size: 12px;
  border-radius: 0.25em;
  border: 1px solid lightgray;
`

export const LoginFormButton = styled.button`
  width: min-content;
  border-radius: 0.25em;
  white-space: nowrap;
  padding: 0.75em;
  border: none;
  background-color: #0077ff;
  color: white;
  font-size: 12px;
`

export const ErrorMessage = styled.p`
  color: red;
  font-size: 12px;
  margin-bottom: 1em;
`