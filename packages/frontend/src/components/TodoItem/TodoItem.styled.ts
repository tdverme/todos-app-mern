import { styled } from "styled-components"

interface TodoItemTextContainerProps {
  completed: boolean
}

export const TodoItemContainer = styled.div<TodoItemTextContainerProps>`
  display: flex;
  align-items: flex-start;
  gap: 1em;
  padding: 1.5em;
  border-radius: 0.5em;

  background-color: ${({ completed }) => completed ? "#EEEEEE" : "white"};
  
  box-shadow: ${({ completed }) => completed ? "none" : "rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px"};
`

export const TodoItemTextContainer = styled.div<TodoItemTextContainerProps>`
  flex: 1;
  
  display: flex;
  flex-direction: column;
  gap: 0.25em;
  align-items: flex-start;

  text-decoration: ${({ completed }) => completed ? "line-through" : "none"};
  color: ${({ completed }) => completed ? "gray" : "black"};
`

export const TodoItemCheckbox = styled.input.attrs({ type: 'checkbox' })`
  margin-top: 0.25em;
`

export const TodoItemTitle = styled.h3`
  font-size: 14px;
`

export const TodoItemDescription = styled.p`
  font-size: 12px;
`

export const TodoItemDeleteButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 1rem;
  height: 1rem;
  border-radius: 0.5rem;
  
  background-color: #FFCBD1;
  font-size: 10px;
  color: #FF2C2C;
`