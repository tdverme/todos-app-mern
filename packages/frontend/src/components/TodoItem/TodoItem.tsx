import { Todo } from "types/Todo"
import { TodoItemCheckbox, TodoItemTextContainer, TodoItemContainer, TodoItemDescription, TodoItemTitle, TodoItemDeleteButton } from "./TodoItem.styled"
import { useState } from "react"

interface TodoItemProps {
  todo: Todo
  onToggle: () => void
  onDelete: () => void
}

const TodoItem = ({ todo, onToggle, onDelete }: TodoItemProps) => {
  const [completed, setCompleted] = useState(todo.completed)

  const handleToggle = () => {
    setCompleted(!completed)
    onToggle()
  }

  return (
    <TodoItemContainer completed={completed}>
      <TodoItemCheckbox checked={completed} onChange={handleToggle} />
      <TodoItemTextContainer completed={completed}>
        <TodoItemTitle>{todo.title}</TodoItemTitle>
        <TodoItemDescription>{todo.description}</TodoItemDescription>
      </TodoItemTextContainer>
      <TodoItemDeleteButton type="button" onClick={onDelete}>x</TodoItemDeleteButton>
    </TodoItemContainer>
  )
}

export default TodoItem