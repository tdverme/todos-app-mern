import { useState } from "react";
import { TodoFormButton, TodoFormContainer, TodoFormInput, TodoFormInputContainer, TodoFormLabel, TodoFormTitle } from "./TodoForm.styled"

interface TodoFormProps {
  onSubmit: (title: string, description: string) => void
}

const TodoForm = ({ onSubmit }: TodoFormProps) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onSubmit(title, description);
    setTitle("");
    setDescription("");
  };

  return (
    <TodoFormContainer onSubmit={handleSubmit}>
      <TodoFormTitle>Crear Tarea</TodoFormTitle>

      <TodoFormInputContainer>
        <TodoFormLabel>Titulo</TodoFormLabel>
        <TodoFormInput id="title" value={title} onChange={(e) => setTitle(e.target.value)} />
      </TodoFormInputContainer>

      <TodoFormInputContainer>
        <TodoFormLabel>Descripcion</TodoFormLabel>
        <TodoFormInput id="description" value={description} onChange={(e) => setDescription(e.target.value)} />
      </TodoFormInputContainer>

      <TodoFormButton type="submit">Crear Tarea</TodoFormButton>
    </TodoFormContainer>
  )
}

export default TodoForm