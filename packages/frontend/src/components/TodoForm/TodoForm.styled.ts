import styled from "styled-components"

export const TodoFormContainer = styled.form`
  width: 100%;
  display: flex;
  flex-direction: column;
  gap: 0.25em;
`

export const TodoFormTitle = styled.h4`
  margin-bottom: 0.75em;
`

export const TodoFormInputContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.25em;
  margin-bottom: 0.5em;
`

export const TodoFormLabel = styled.label`
  font-size: 10px;
  font-weight: normal;
  color: #2F2F2F;
`

export const TodoFormInput = styled.input`
  width: 250px;
  padding: 0.5em;
  font-size: 12px;
  border-radius: 0.25em;
  border: 1px solid lightgray;
`

export const TodoFormButton = styled.button`
  width: min-content;
  border-radius: 0.25em;
  white-space: nowrap;
  padding: 0.75em;
  border: none;
  background-color: #0077ff;
  color: white;
  font-size: 12px;
`