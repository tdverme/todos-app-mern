import { styled } from "styled-components";

export const TodoItemListContainer = styled.div`
  width: 100%;

  display: flex;
  flex-direction: column;
  gap: 0.75em;
`

export const TodoItemListTitle = styled.h4`
  margin-bottom: 0.25em;
`