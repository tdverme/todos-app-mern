/* eslint-disable @typescript-eslint/no-misused-promises */
import { Todo } from "types/Todo"
import { TodoItemListContainer, TodoItemListTitle } from "./TodoItemList.styled"
import TodoItem from "components/TodoItem/TodoItem"
import axios, { AxiosResponse } from "axios"
import { compareTodos } from "utilities/compareTodos"

interface TodoItemListProps {
  todos: Todo[]
  setTodos: React.Dispatch<React.SetStateAction<Todo[]>>
}

const TodoItemList = ({ todos, setTodos }: TodoItemListProps) => {
  const jwt = localStorage.getItem("token");

  const handleToggleItem = async (todo: Todo) => {
    const { data: todoUpdated } = await axios.put<Todo, AxiosResponse<Todo>, Partial<Todo>>(
      `/api/todos/${todo._id}`,
      { completed: !todo.completed },
      { headers: { Authorization: `Bearer ${jwt || ""}` } }
    )
    setTodos((prev) => [...prev.filter((t) => t._id !== todo._id), todoUpdated].sort(compareTodos))
  }

  const handleDeleteItem = async (todo: Todo) => {
    await axios.delete(
      `/api/todos/${todo._id}`,
      { headers: { Authorization: `Bearer ${jwt || ""}` } }
    )
    setTodos((prev) => [...prev.filter((t) => t._id !== todo._id)])
  }

  return (
    <TodoItemListContainer>
      <TodoItemListTitle>Mis Tareas</TodoItemListTitle>
      {todos.map((todo) => (
        <TodoItem
          key={todo._id}
          todo={todo}
          onToggle={async () => await handleToggleItem(todo)}
          onDelete={async () => await handleDeleteItem(todo)}
        />
      ))}
    </TodoItemListContainer>
  )
}

export default TodoItemList