import TodosPage from "pages/Home/Home.page"
import "./globals.css"
import LoginPage from "pages/Login/Login.page"
import { BrowserRouter, Routes as ReactRoutes, Route } from "react-router-dom"

const App = (): React.ReactElement => {
  return (
    <BrowserRouter>
      <ReactRoutes>
        <Route path='/login' element={<LoginPage />} />
        <Route path='/' element={<TodosPage />} />
      </ReactRoutes>
    </BrowserRouter >
  )
}

export default App