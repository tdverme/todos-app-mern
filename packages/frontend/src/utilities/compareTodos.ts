import { Todo } from "types/Todo"

export const compareTodos = (a: Todo, b: Todo) => {
  return (a._id > b._id) ? -1 : ((b._id > a._id) ? 1 : 0)
}