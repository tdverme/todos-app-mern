import { IUser, User } from "../models/user.model"
import express, { NextFunction, Request, Response } from "express"
import jsonwebtoken from "jsonwebtoken"
import bcrypt from "bcrypt"

export const userRouter = express.Router()

interface ILoginRequestBody {
  username: string,
  password: string
}

interface ILoginResponseBody {
  token?: string,
}

interface IErrorReponseBody {
  message: string
}


// ../api/login

userRouter.post('/', async (req: Request<{}, any, ILoginRequestBody>, res: Response<ILoginResponseBody | IErrorReponseBody>, next: NextFunction): Promise<void> => {
  try {
    const { username, password } = req.body

    const userDB = await User.findOne({ username })

    if (!userDB) {
      console.error(`'${username}' no existe en la base de datos`)

      res.status(401).json({
        message: 'Usuario o contraseña incorrectos'
      })

      return
    }

    const shouldLogin = await bcrypt.compare(password, userDB.passwordHash)

    if (!shouldLogin) {
      console.error(`'${username}' intentó loguearse con credenciales incorrectas`)

      res.status(401).json({
        message: 'Usuario o contraseña incorrectos'
      })

      return
    }

    if (!process.env.JWT_SECRET) {
      console.error(`Secreto JWT no encontrado`)

      res.status(500).json({
        message: 'Internal Server Error'
      })

      return
    }

    const userToSign = {
      id: userDB._id,
    }

    console.log(`'${username}' logueado`)

    const token = jsonwebtoken.sign(userToSign, process.env.JWT_SECRET)
    res.status(200).json({
      token
    })
  } catch (err: any) {
    console.error(`Error intentando loguear usuario: ${err.message}`)
    next(err)
  }
})

export const registerUser = async (user: Omit<IUser, "_id">): Promise<IUser> => {
  const newUser = new User({ ...user, createdAt: new Date() })
  const savedUser = await newUser.save()
  return savedUser
}