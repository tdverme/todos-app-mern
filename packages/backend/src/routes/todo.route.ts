import { Router } from "express"
import { ITodo, Todo } from "../models/todo.model"

// /api/todos
const todoRouter = Router()

// Obtener todas las tareas
todoRouter.get("/", async (req, res) => {
  try {
    const todos = await Todo.find({})
    res.json(todos)
  } catch {
    console.log('Error al obtener las tareas')
    res.status(400)
  }
})

// Obtener una tarea especifica
todoRouter.get("/:id", async (req, res) => {
  try {
    const id = req.params.id
    const todo = await Todo.findById({ _id: id })
    res.json(todo)
  } catch {
    console.log('Error al obtener la tarea')
    res.status(400)
  }
})

// Crear una tarea nueva
todoRouter.post<{}, {}, ITodo>("/", async (req, res) => {
  try {
    const info = req.body

    const todo = new Todo(info)

    const savedTodo = await todo.save()

    res.json(savedTodo)
  } catch {
    console.log('Error al crear tarea')
    res.status(400)
  }
})

// Actualización una tarea
todoRouter.put<{ id: string }, ITodo | null, ITodo>("/:id", async (req, res) => {
  try {
    const id = req.params.id
    const info = req.body

    const todo = await Todo.findOneAndUpdate({ _id: id }, info, { new: true })

    res.json(todo)
  } catch {
    console.log('Error al actualizar tarea')
    res.status(400)
  }
})

// Eliminar una tarea
todoRouter.delete("/:id", async (req, res) => {
  try {
    const id = req.params.id

    const todo = await Todo.findByIdAndDelete({ _id: id })

    res.json(todo)
  } catch {
    console.log('Error al actualizar tarea')
    res.status(400)
  }
})

export default todoRouter