import express from "express"
import cors from "cors"
import bodyParser from "body-parser"
import { connect, connection } from "mongoose"
import todoRouter from "./routes/todo.route"
import { userRouter } from "./routes/user.route"
import dotenv from 'dotenv'
import { authenticate } from "./middlewares/authenticate.middleware"

dotenv.config()

const { MONGO_USER, MONGO_PW, MONGO_DB } = process.env

const app = express()

const port = 5000

// Middlewares
app.use(cors())

app.use(bodyParser.json())

app.use("/api/login", userRouter)
app.use("/api/todos", authenticate, todoRouter)

// Conexión a MongoDB
connect(`mongodb+srv://${MONGO_USER}:${MONGO_PW}@cluster0.auewzo5.mongodb.net/${MONGO_DB}?retryWrites=true&w=majority`)
connection.once("open", () => {
  console.log("Conectado a Mongo")
})

// Inicio server
app.listen(port, () => {
  console.log("Server corriendo")
})