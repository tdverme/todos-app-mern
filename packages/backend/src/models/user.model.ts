import mongoose, { Schema } from "mongoose"
import { model } from "mongoose"

export interface IUser {
  _id: mongoose.Types.ObjectId,
  username: string
  passwordHash: string
}

const userSchema = new Schema<IUser>({
  username: {
    type: String,
    unique: true,
    required: true
  },
  passwordHash: {
    type: String,
    required: true
  }
})

userSchema.set('toJSON', {
  virtuals: true,
  transform: (_, returnedObject) => {
    delete returnedObject.passwordHash
  }
})

export const User = model<IUser>('User', userSchema)
