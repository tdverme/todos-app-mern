# Capacitación MERN - Todos App

## Configuración

Crear un archivo `.env` en `packages/backend` con los siguientes datos:

```
MONGO_USER=[Usuario en MongoDB Atlas]
MONGO_PW=[Contraseña en MongoDB Atlas]
MONGO_DB=todos-app
JWT_SECRET=12345
```
